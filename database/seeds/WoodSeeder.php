<?php

use App\Category;
use App\Supplier;
use Illuminate\Database\Seeder;

class WoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();
        $suppliers = Supplier::all();

        DB::table('wood')->insert([
            [
                'name' => 'Glue Laminated Wood',
                'length' => '200x150',
                'price' => '152',
                'category_id' => $categories->where('name', 'construction')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Rame Trees')->first()->id,
            ],
            [
                'name' => 'Thin pine log',
                'length' => '200x25',
                'price' => '458',
                'category_id' => $categories->where('name', 'construction')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Rame Trees')->first()->id,
            ],
            [
                'name' => 'Thick pine log',
                'length' => '200x25',
                'price' => '58',
                'category_id' => $categories->where('name', 'construction')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Rame Trees')->first()->id,
            ],
            [
                'name' => 'Douglas',
                'length' => '200x20',
                'price' => '6',
                'category_id' => $categories->where('name', 'construction')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Rame Trees')->first()->id,
            ],
            [
                'name' => 'Finger Joint',
                'length' => '250x30',
                'price' => '88',
                'category_id' => $categories->where('name', 'construction')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Rame Trees')->first()->id,
            ],
            [
                'name' => 'pine planed',
                'length' => '100x25',
                'price' => '94',
                'category_id' => $categories->where('name', 'deck')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Hadarim Wood Industries')->first()->id,
            ],
            [
                'name' => 'Pine deck',
                'length' => '100x10',
                'price' => '546',
                'category_id' => $categories->where('name', 'deck')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Hadarim Wood Industries')->first()->id,
            ],
            [
                'name' => 'Tabebuia',
                'length' => '200x15',
                'price' => '25',
                'category_id' => $categories->where('name', 'deck')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Hadarim Wood Industries')->first()->id,
            ],
            [
                'name' => 'Fused Bamboo',
                'length' => '180x20',
                'price' => '375',
                'category_id' => $categories->where('name', 'deck')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Hadarim Wood Industries')->first()->id,
            ],
            [
                'name' => 'Iroko',
                'length' => '200x15',
                'price' => '374',
                'category_id' => $categories->where('name', 'deck')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Hadarim Wood Industries')->first()->id,
            ],
            [
                'name' => 'Sanitized pine',
                'length' => '150x25',
                'price' => '159',
                'category_id' => $categories->where('name', 'pergola')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Tubul&Sons')->first()->id,
            ],
            [
                'name' => 'Red Ceder',
                'length' => '150x15',
                'price' => '758',
                'category_id' => $categories->where('name', 'pergola')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Erez Wood Storage and Tiles')->first()->id,
            ],
            [
                'name' => 'Tectona Grandis',
                'length' => '200x20',
                'price' => '8',
                'category_id' => $categories->where('name', 'pergola')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Hadarim Wood Industries')->first()->id,
            ],
            [
                'name' => 'French Spruce',
                'length' => '200x15',
                'price' => '74',
                'category_id' => $categories->where('name', 'pergola')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Tubul&Sons')->first()->id,
            ],
            [
                'name' => 'Pacific Coast Hemloc',
                'length' => '150x15',
                'price' => '556',
                'category_id' => $categories->where('name', 'pergola')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Erez Wood Storage and Tiles')->first()->id,
            ],
            [
                'name' => 'Narrow Notepader',
                'length' => '150x20',
                'price' => '897',
                'category_id' => $categories->where('name', 'parquet')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Tubul&Sons')->first()->id,
            ],
            [
                'name' => 'Wide Notepader',
                'length' => '150x20',
                'price' => '596',
                'category_id' => $categories->where('name', 'parquet')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Tubul&Sons')->first()->id,
            ],
            [
                'name' => 'Thick Narrow Notepad',
                'length' => '150x15',
                'price' => '25',
                'category_id' => $categories->where('name', 'parquet')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Erez Wood Storage and Tiles')->first()->id,
            ],
            [
                'name' => 'Thick Wide Notepader',
                'length' => '150x30',
                'price' => '745',
                'category_id' => $categories->where('name', 'parquet')->first()->id,
                'supplier_id' => $suppliers->where('name', 'Erez Wood Storage and Tiles')->first()->id,
            ],
        ]);
    }
}