<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@gagot.com',
                'email_verified_at' => now(),
                'password' => Hash::make(12345678),
                'role_id' => Role::where('name', 'admin')->first()->id,
                'remember_token' => Str::random(10),
            ],
            [
                'name' => 'user',
                'email' => 'user@gagot.com',
                'email_verified_at' => now(),
                'password' => Hash::make(12345678),
                'role_id' => Role::where('name', 'user')->first()->id,
                'remember_token' => Str::random(10),
            ],
        ]);
        factory(User::class, 5)->create();
    }
}