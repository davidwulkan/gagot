@extends('layouts.app')

@section('title','Customers')

@section('content')
    <div>
        <a href="{{url('/customers/create')}}" class="btn btn-info">Add new customer</a>
        <h1>List of Customers</h1>
    </div>

    <table class="table">
        <tr>
            <th>@sortablelink('name')</th>
            <th></th>
            <th></th>
        </tr>

        @foreach($customers as $customer)
            <tr>
                <td>{{$customer->name}}</td>
                <td>
                    <a href="{{route('customers.edit',$customer->id)}}">Edit</a>
                </td>
                <td>
                    <a href="{{route('customers.delete',$customer->id)}}">Delete</a>
                </td>
            </tr>

        @endforeach
    </table>
    {{$customers->withQueryString()->links()}}
@endsection
                        