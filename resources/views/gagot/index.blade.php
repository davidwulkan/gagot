@extends('layouts.app')

@section('title','Sales')

@section('content')
    <div class="row justify-content-around">
        <div class="row">
            <a href="{{url('/gagot/create')}}" class="btn btn-info">Add new sale</a>
        </div>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">
                Filter by worker
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($users as $user)
                    <a class="dropdown-item"
                       href="{{route('gagot.index')}}?user={{$user->id}}">{{$user->name}}</a>
                @endforeach
            </div>
        </div>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">
                Filter by customer
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($customers as $customer)
                    <a class="dropdown-item"
                       href="{{route('gagot.index')}}?customer={{$customer->id}}">{{$customer->name}}</a>
                @endforeach
            </div>
        </div>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">
                Filter by wood
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($wood as $single_wood)
                    <a class="dropdown-item"
                       href="{{route('gagot.index')}}?wood={{$single_wood->id}}">{{$single_wood->name}}</a>
                @endforeach
            </div>
        </div>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">
                Filter by status
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($statuses as $status)
                    <a class="dropdown-item"
                       href="{{route('gagot.index')}}?status={{$status->id}}">{{$status->name}}</a>
                @endforeach
            </div>
        </div>

    </div>
        <h1>List of Sales</h1>

    <table class="table">
        <tr>
            <th>id</th>
            <th>@sortablelink('supply_date')</th>
            @if(auth()->user()->role->name === 'admin')
                <th>Owner</th>
            @endif
            <th>@sortablelink('customer_id', 'Customer')</th>
            <th>@sortablelink('wood_id','Wood')</th>
            <th>@sortablelink('amount')</th>
            <th>Total Price</th>
            <th>@sortablelink('status_id','Status')</th>
            <th></th>
            <th></th>
        </tr>

        @foreach($sales as $sale)
            <tr>
                <td>{{$sale->id}}</td>
                <td>{{$sale->supply_date}}</td>
                @if(auth()->user()->role->name === 'admin')
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle"
                                    type="button"
                                    id="dropdownMenuButton"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false">
                                @if(isset($sale->user_id))
                                    {{$sale->owner->name}}
                                @else
                                    Select owner
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($users as $user)
                                    <a class="dropdown-item"
                                       href="{{route('gagot.changeuser',[$sale->id,$user->id])}}">{{$user->name}}</a>
                                @endforeach
                            </div>
                        </div>
                    </td>
                @endif
                <td>{{$sale->customer->name}}</td>
                <td>{{$sale->wood->name}}</td>
                <td>{{$sale->amount}}</td>
                <td>{{$sale->amount * $sale->wood->price}}</td>

                <td>
                    <div class="dropdown">
                        @if (App\Status::next($sale->status_id) != null )
                        <button class="btn btn-secondary dropdown-toggle"
                        @else
                        <button class="btn btn-secondary"
                        @endif
                                type="button"
                                id="dropdownMenuButton"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            @if (isset($sale->status_id))
                                {{$sale->status->name}}
                            @else
                                Define status
                            @endif
                        </button>
                        @if (App\Status::next($sale->status_id) != null )
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach(App\Status::next($sale->status_id) as $status)
                                    <a class="dropdown-item"
                                       href="{{route('gagot.changestatus', [$sale->id,$status->id])}}">{{$status->name}}</a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </td>


                <td>
                    <a href="{{route('gagot.edit',$sale->id)}}">Edit</a>
                </td>
                <td>
                    <a href="{{route('gagot.delete',$sale->id)}}">Delete</a>
                </td>
            </tr>

        @endforeach
    </table>
    {{$sales->withQueryString()->links()}}
@endsection
                        