@extends('layouts.app')

@section('title','Create new Sale')

@section('content')

    <h1>Create Sale</h1>
    <form method="post" action="{{action('GagotController@store')}}">
        @csrf
        <div class="form-group">
            <label for="customer_id">Customer</label>
            <select class="form-control" name="customer_id">
                @foreach($customers as $customer)
                    <option value="{{$customer->id}}">{{$customer->name}}</option>
                @endforeach
            </select>
        </div>
        @if(auth()->user()->role->name === 'admin')
            <div class="form-group">
                <label for="user_id">User</label>
                <select class="form-control" name="user_id">
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
        @endif
        <div class="form-group">
            <label for="wood_id">Wood</label>
            <select class="form-control" name="wood_id">
                @foreach($wood as $singleWood)
                    <option value="{{$singleWood->id}}">{{$singleWood->name}}, Price: {{$singleWood->price}}, Length: {{$singleWood->length}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="amount">Amount</label>
            <input type="text" class="form-control" name="amount">
        </div>
        <div>
            <label for="supply_date">Supply Date</label>
            <input type="date" class="form-control" name="supply_date">
        </div>
        <div>
            <input type="submit" name="submit" value="Create Sale">
        </div>
    </form>
@endsection
