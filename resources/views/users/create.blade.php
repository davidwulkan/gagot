@extends('layouts.app')

@section('title','Create new User')

@section('content')

    <h1>Create User</h1>
    <form method="post" action="{{action('UserController@store')}}">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password">
        </div>
        <div class="form-group">
            <label for="role_id">Role</label>
            <select class="form-control" name="role_id">
                @foreach($roles as $role)
                    <option value="{{$role->id}}">{{$role->name}}</option>
                @endforeach
            </select>
        </div>
        <div>
            <input type="submit" name="submit" value="Create User">
        </div>
    </form>
@endsection
