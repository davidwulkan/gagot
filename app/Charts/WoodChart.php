<?php

declare(strict_types=1);

namespace App\Charts;

use App\Sale;
use App\Wood;
use Carbon\Carbon;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class WoodChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $wood = Sale::all()->groupBy('wood_id')->map(function ($sale) {
            return $sale->sum('amount');
        });
        $keys = $wood->keys()->map(function ($key){
            return Wood::find($key)->name;
        });
        return Chartisan::build()
            ->labels($keys->toArray())
            ->dataset('Wood Sales By Type', $wood->values()->toArray());
    }
}